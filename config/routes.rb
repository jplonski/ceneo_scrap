CeneoScrapper::Application.routes.draw do
  root :to => "home#index"

  put "/set_current_shop", to: "home#set_current_shop", as: :set_current_shop

  resources :producers do
    get :products, to: "products#producer_products_index"
    collection do
      post :import
    end
  end

  resources :collections do
    get :products, to: "products#collection_products_index"
    collection do
      post :import
    end
  end

  resources :products do
    member do
      get :refresh
    end
    collection do
      get :refresh_collection
    end
  end
end

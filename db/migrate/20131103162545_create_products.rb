class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.text :base_list_url
      t.text :remaining_list_url
      t.string :listing_id
      t.belongs_to :producer
      t.belongs_to :category
      t.belongs_to :collection

      t.timestamps
    end
    add_index :products, :producer_id
    add_index :products, :category_id
    add_index :products, :collection_id
  end
end

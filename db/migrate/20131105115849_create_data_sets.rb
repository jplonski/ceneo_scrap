class CreateDataSets < ActiveRecord::Migration
  def change
    create_table :data_sets do |t|
      t.belongs_to :product

      t.timestamps
    end
    add_index :data_sets, :product_id
  end
end

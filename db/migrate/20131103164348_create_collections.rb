class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name
      t.text :url
      t.belongs_to :producer

      t.timestamps
    end
    add_index :collections, :producer_id
  end
end

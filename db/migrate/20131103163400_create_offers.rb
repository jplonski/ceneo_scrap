class CreateOffers < ActiveRecord::Migration
  def change
    create_table :offers do |t|
      t.belongs_to :data_set
      t.belongs_to :shop
      t.integer :position
      t.integer :price
      t.text :url

      t.timestamps
    end
    add_index :offers, :shop_id
    add_index :offers, :data_set_id
  end
end

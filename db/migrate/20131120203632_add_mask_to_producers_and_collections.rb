class AddMaskToProducersAndCollections < ActiveRecord::Migration
  def change
    add_column :producers, :mask, :text
    add_column :producers, :page_count, :integer
    add_column :collections, :mask, :text
    add_column :collections, :page_count, :integer
    add_column :collections, :make_id, :integer
  end
end

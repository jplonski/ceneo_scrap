class CreateProducers < ActiveRecord::Migration
  def change
    create_table :producers do |t|
      t.text :url
      t.string :kh_kod
      t.string :name

      t.timestamps
    end
  end
end

class CreateMakes < ActiveRecord::Migration
  def change
    create_table :makes do |t|
      t.belongs_to :producer
      t.string :name
      t.text :url
      t.integer :page_count
      t.text :mask

      t.timestamps
    end
    add_index :makes, :producer_id
  end
end

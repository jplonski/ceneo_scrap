class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.text :home_url
      t.text :listing_url
      t.text :site_specific_id

      t.timestamps
    end
  end
end

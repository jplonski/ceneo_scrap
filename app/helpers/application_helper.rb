module ApplicationHelper

  def set_title
    controler = params[:controller]
    case controler
    when "products"
      @title = "Produkty"
    when "producers"
      @title = "Producenci"
    when "collections"
      @title = "Kolekcje"
    when "home"
      @title = "Home"
    else
      @title = params[:controller]
    end
    content_for(:title) do
      @title
    end
  end

end

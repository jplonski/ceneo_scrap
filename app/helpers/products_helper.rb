module ProductsHelper

  def price_spread(prices)
    [prices[0], prices[-1]]
  end

  def position_for_shop(shop, prices)
    return nil unless prices
    index = index_for_shop(shop, prices)
    index ? index + 1 : "brak"
  end

  def difference_from_best(shop, prices)
    return nil unless prices
    index = index_for_shop(shop, prices)
    return nil unless index
    ((prices[index][1] - prices[0][1]) / 100.0).to_s + " zł"
  end

  def percentage_difference_from_best(shop, prices)
    return nil unless prices
    index = index_for_shop(shop, prices)
    return nil unless index
    (((((prices[index][1] * 1.0) / prices[0][1])) - 1) * 100).round(1).to_s + " %"
  end

  def index_for_shop(shop, prices)
    prices.index{ |price| price[0] == shop.name } 
  end

end

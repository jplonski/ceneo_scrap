class CollectionsController < ApplicationController
  
  def index
    @collections = Collection.order("name asc").all
  end

  def new
    @collection = Collection.new
    render "show"
  end

  def create
    @collection = Collection.new(params[:collection])
    if @collection.save
      redirect_to collections_path, notice: "Stworzono nową kolekcję - #{@collection.name}"
    else
      @collection
      flash.now[:alert] = "Popraw dane"
      render "show"
    end
  end

  def show
    @collection = Collection.find(params[:id])
  end

  def edit
    @collection = Collection.find(params[:id])
    render "show"
  end

  def update
    @collection = Collection.find(params[:id])
    if @collection.update_attributes(params[:collection])
      redirect_to collections_path, notice: "Zaktualizowano kolekcję - #{@collection.name}"
    else
      @collection
      flash.now[:alert] = "Popraw dane"
      render "show"
    end
  end

  def destroy
    Collection.find(params[:id]).destroy
    redirect_to collections_path, notice: "Kolekcja usunięta"
  end

  def import
    Collection.import(params[:file])
    redirect_to collections_url, notice: "Kolekcje zaimportowane"
  end
end

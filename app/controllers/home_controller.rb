class HomeController < ApplicationController

  def index
    #set_current_shop unless session[:current_shop_id]
    @shops = Shop.order("name ASC").all
  end

  def set_current_shop
    session[:current_shop_id] = params[:shop_id] || Shop.find_by_name("Eplytki.pl").try(:id)
    render json: { path: request.referrer }
  end
end

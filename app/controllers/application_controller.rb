class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_title

  private

  def set_title
    controler = params[:controller]
    case controler
    when "products"
      if params[:action] == "show"
        @title = "Produkt"
      else
        @title = "Produkty"
      end
    when "producers"
      @title = "Producenci"
    when "collections"
      @title = "Kolekcje"
    when "home"
      @title = "Home"
    else
      @title = params[:controller]
    end
  end
end

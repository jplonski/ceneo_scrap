class ProductsController < ApplicationController

  def index
    respond_to do |format|
      format.html { @products, @alphaParams = Product.order("name asc").all.alpha_paginate(params[:letter]){ |product| product.name} }
      format.xls  { @products = Product.order("name asc") }
    end
  end

  def show
    @product = Product.includes(:producer, :collection, :data_sets).find(params[:id])
    @data_sets = @product.data_sets.order("created_at DESC").limit(10).reverse
    @products_from_collection = @product.products_from_collection
    @similar_products = Product.similar_products_for(@product)
  end

  def update
    @product = Product.find(params[:id])
    attrs = case params[:commit]
            when "Zmień producenta"
              params[:product].except(:collection_id)
            when "Zmień kolekcję"
              params[:product].except(:producer_id)
            end
    if @product.update_attributes(attrs)
      if params[:with_selected_products]
        update_selected_products(params[:products_to_update].split(","), attrs)
      end
      redirect_to product_path(@product), notice: "Zapisano zmiany w produkcie"
    else
      @product
      flash.now[:error] = "Nie można zapisać zmian"
      render 'show'
    end
  end

  def producer_products_index
    @producer = Producer.find(params[:producer_id])
    @products = @producer.products.order("products.name asc").all
    render :index
  end

  def collection_products_index
    @collection = Collection.find(params[:collection_id])
    @products = @collection.products.order("products.name asc").all
    render :index
  end

  def refresh
    product = Product.find(params[:id])
    product.data_sets.create.offers << Offer.fetch_offers_for(product, true)
    find_proper_collection
    if @products
      render :index
    else
      redirect_to product_path(product)
    end
  end

  def refresh_collection
    find_proper_collection
    Offer.fetch_offers_for(@products, true) 
    find_proper_collection
    render :index
  end

  def destroy
    Producer.find(params[:id]).destroy
    redirect_to products_path, notice: "Producent usunięty"
  end

  def find_proper_collection
    if params[:producer_id].presence
      @producer = Producer.find(params[:producer_id])
      @products = @producer.products.order("products.name asc").all
    end
    if params[:collection_id].presence
      @collection = Collection.find(params[:collection_id])
      @products = @collection.products.order("products.name asc").all
    end
    if params[:letter].presence
      @products, @alphaParams = Product.order("name asc").all.alpha_paginate(params[:letter]){ |product| product.name} if params[:letter]
    end
  end

  private

  def update_selected_products(ids, attrs)
    ids.each do |product_id|
      unless Product.find(product_id).update_attributes(attrs)
        flash.now[:error] = "Nie można zapisać zmian dla jednego z zaznaczonych produktów o id: #{product_id} - produkt który widzisz na tej stronie został jednak zapisany!"
        render 'show' and return
      end
    end
  end

end

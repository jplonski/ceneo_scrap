class ProducersController < ApplicationController
  
  def index
    @producers = Producer.order("name asc").all
  end

  def new
    @producer = Producer.new
    render "show"
  end

  def create
    @producer = Producer.new(params[:producer])
    if @producer.save
      redirect_to producers_path, notice: "Stworzono nowego producenta - #{@producer.name}"
    else
      @producer
      flash.now[:alert] = "Popraw dane"
      render "show"
    end
  end

  def show
    @producer = Producer.find(params[:id])
  end

  def edit
    @producer = Producer.find(params[:id])
    render "show"
  end

  def update
    @producer = Producer.find(params[:id])
    if @producer.update_attributes(params[:producer])
      redirect_to producers_path, notice: "Zaktualizowano producenta - #{@producer.name}"
    else
      @producer
      flash.now[:alert] = "Popraw dane"
      render "show"
    end
  end

  def destroy
    Producer.find(params[:id]).destroy
    redirect_to producers_path, notice: "Producent usunięty"
  end

  def import
    Producer.import(params[:file])
    redirect_to producers_url, notice: "Producenci zaimportowani"
  end
end

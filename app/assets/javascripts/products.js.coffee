class SelectProducts
  constructor: ->
    @elems = $(".products-select")
    @value_store_elem = $("#products_to_update")
    @bindings()

  set_order_ids: (event) =>

    tableVal = @elems.map((i, v) ->
      $(this).val()
      ).toArray().unique()
    @value_store_elem.val(tableVal)

  bindings: ->
    $(".products-select").on "change", @set_order_ids
jQuery ->
  new SelectProducts if $("#product").length > 0

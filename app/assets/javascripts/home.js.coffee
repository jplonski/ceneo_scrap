# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
  $("#shops").on "change", -> 
    shop_id = $(this).val()
    $.ajax(
      url: "/set_current_shop"
      type: "PUT"
      data: { shop_id: shop_id }
    ).always (msg) ->
      window.location = msg.path

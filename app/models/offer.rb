class Offer < ActiveRecord::Base

  extend CeneoFetcher::OffersFetcher

  belongs_to :product
  belongs_to :data_set
  belongs_to :shop

  attr_accessible :position, 
                  :price
end

class DataSet < ActiveRecord::Base
  belongs_to :product
  has_many :offers, dependent: :destroy

  default_scope order("created_at DESC")

  scope :latest, limit(1)

  def self.latest_one
    self.latest.first
  end

  def prices
    prices = self.offers.map{ |offer| [offer.shop.name, offer.price] }.sort_by{ |offer| offer[1] }
  end

end

class Make < ActiveRecord::Base
  belongs_to :producer
  has_many :products

  attr_accessible :mask,
                  :name,
                  :page_count,
                  :url

  def to_s
    self.name
  end
end

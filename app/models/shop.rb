class Shop < ActiveRecord::Base
  has_many :offers, dependent: :destroy

  attr_accessible :home_url, :name, :site_specific_id, :listing_url

  validates :name, uniqueness: true

  def to_s
    self.name
  end
end

class Collection < ActiveRecord::Base
  belongs_to :producer
  belongs_to :make
  has_many :products

  attr_accessible :mask,
                  :name,
                  :page_count,
                  :url

  validates :name, uniqueness: true

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Collection.create! row.to_hash
    end
  end

  def to_s
    self.name
  end
end

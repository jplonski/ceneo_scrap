class Category < ActiveRecord::Base
  
  has_many :collections
  has_many :products

  attr_accessible :name, 
                  :url

  validates :name, uniqueness: true
end

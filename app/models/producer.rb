class Producer < ActiveRecord::Base

  extend  CeneoFetcher::ProducersFetcher

  has_many :makes
  has_many :collections
  has_many :products

  attr_accessible :kh_kod,
                  :mask,
                  :name,
                  :page_count

  validates :name, uniqueness: true

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Producer.create row.to_hash
    end
  end

  def to_s
    self.name
  end
end

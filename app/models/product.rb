class Product < ActiveRecord::Base

  extend CeneoFetcher::ProductsFetcher

  belongs_to :producer
  belongs_to :category
  belongs_to :collection
  has_many :data_sets

  attr_accessible :base_list_url,
                  :collection_id,
                  :listing_id,
                  :mask,
                  :name,
                  :producer_id,
                  :remaining_list_url

  validates :name, uniqueness: true

  def match_producer
    self.producer = find_producer
    self.save
    self.reload
  end

  def latest_offers
    return nil unless self.data_sets.exists?
    self.data_sets.latest_one.offers
  end

  def to_s
    self.name
  end

  def products_from_collection
    return [] unless self.collection
    #TODO - refactor
    self.collection.products.where("id != ?", self.id).order("LOWER(name)")
  end

  def self.similar_products_for(product)
    #TODO - refactor
    ret = []
    ret << Product.where("name ilike ?", "#{product.name.split(/\s+/, 5)[0...4].join(' ')}%").all - [product]
    ret << Product.where("name ilike ?", "#{product.name.split(/\s+/, 4)[0...3].join(' ')}%").all - ret.flatten - [product]
    ret << Product.where("name ilike ?", "#{product.name.split(/\s+/, 3)[0...2].join(' ')}%").all - ret.flatten - [product]
    ret.flatten
  end

  private

  def find_producer
    producer = self.class.match_product_producer_from(self.name)
  end
end

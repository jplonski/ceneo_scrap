module CeneoFetcher::ProductsFetcher

  def fetch_product(input)
    parsed_name = input.search("a").first["title"]
    #puts "parsed_name #{parsed_name}"
    #prods = producers.select{ |prod| parsed_name.upcase.include?(prod.name) }
    producer = match_product_producer_from(parsed_name)
    #puts "producer #{producer}"
    product = Product.new name: parsed_name, base_list_url: input.search("a").last["href"]
    product.listing_id = product.base_list_url.gsub("/","")
    product.remaining_list_url = product.base_list_url + "clr-"
    if producer
      product.producer = producer
    end
    #colls = collections.select{ |col| parsed_name.split(" ").select{ |name| name.upcase == col.name }.any? }
    parsed_name = remove_producer_from(parsed_name, producer) if producer
    collection = match_collection_from(parsed_name, producer)
    #puts "collection #{collection}"
    if collection
      product.collection = collection
    end
    #puts "valid? #{product.valid?}"
    product.save
  end

  def match_all_product_producers_from(parsed_name)
    Matchers::ProducerMatcher.match_all_from(parsed_name)
  end

  def match_product_producer_from(parsed_name)
    producers = self.match_all_product_producers_from(parsed_name)
    if producers.any?
      producers.all.sort_by! do |producer|
        (producer.name.split(" ") & parsed_name.split(" ")).size
      end
      producers.last
    else
      Producer.find_or_create_by_name("Pozostali")
    end
  end

  def match_all_collections_from(parsed_name)
    Matchers::CollectionMatcher.match_all_from(parsed_name)
  end

  def match_collection_from(parsed_name, producer)
    collections = self.match_all_collections_from(parsed_name)
    if collections.any?
      collections.sort_by!{ |c| c.name.length }
      if producer
        try_match_best_which_collection_and_producer(collections, producer)
      else
        collections.last
      end
    else
      nil
    end
  end

  def remove_producer_from(parsed_name, producer)
    parsed_name.upcase.gsub(producer.name.upcase, "")
  end

  #TODO - refactor
  def try_match_best_which_collection_and_producer(collections, producer)
    producer_name = producer.name
    ret = collections.select do |c|
      c.name.upcase.split("/").include?(producer_name.upcase)
    end.first
    ret = collections.select do |c|
      !c.name.include?("/")
    end.last unless ret
    ret
  end
end

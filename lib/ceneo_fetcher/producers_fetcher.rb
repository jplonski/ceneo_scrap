module CeneoFetcher::ProducersFetcher

  def fetch_all(category = "Plytki")
    agent = Mechanize.new
    agent.user_agent_alias = "Windows Mozilla"
    page = agent.get( [ROADMAP[:ceneo][:url], category].join("/") )
    fetch_all_producers_from(page)
  end

  def fetch_all_producers_from(page)
    producers_data = page.search(".multiple a.js_filter-trigger").map{ |prod| [prod.text, prod[:href]] }
    producers_data.each do |producer_ary|
      producer = Producer.find_or_initialize_by_name(producer_ary.first )
      producer.url = producer_ary.last
      if producer.save
        producer.new_record? ? puts("Nowy producent!   #{producer.name}") : puts("Uaktualniony adres dla producenta  #{producer.name}")
      else
        puts "Wystąpił jakiś błąd przy producencie: #{producer.name}"
      end
    end
  end
end

module CeneoFetcher::OffersFetcher

  def fetch_offers_for(subject, single = false)
    products = []
    if passed_array = subject.is_a?(Array)
      offers = nil
      products += subject
    else
      offers = []
      products << subject
    end
    agent = Mechanize.new
    agent.user_agent_alias = "Windows Mozilla"
    products.each do |product|
      data_set = product.data_sets.create unless offers
      pages = [agent.get( [ROADMAP[:ceneo][:url], product.base_list_url].join ), agent.get( [ROADMAP[:ceneo][:url], product.remaining_list_url].join )]
      pages.each do |page|
        offers << self.fetch_offers_from(page) if offers
        data_set.offers << self.fetch_offers_from(page) unless offers
        sleep(rand(3)) unless single
      end
    end
    return offers
  end


  def fetch_offers_from(page)
    offers = []
    items = page.search("a.store-logo", "a.product-price").sort_by{ |el| el["href"]}
    items.each_slice(2) do |item|
      shop_name_container = item.select{ |el| el["class"].split(" ").include?("store-logo")}.first
      shop_name = 
        if elem = shop_name_container.search("img").first
          shop_listing_id = elem["data-original"].scan( /\d+/ ).first
          elem["alt"] 
        else
          shop_name_container.search("span").first.text
        end
      shop = Shop.find_or_create_by_name( name: shop_name, site_specific_id: shop_listing_id)
      price = item.select{ |el| el["class"].split(" ").include?("product-price")}.first.search("strong").first.text.gsub(",", "").to_i
      offers << shop.offers.create( price: price)
    end
    offers
  end

end

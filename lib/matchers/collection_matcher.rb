module Matchers::CollectionMatcher

  def self.match_all_from(input)
    words = input.upcase.split(" ")
    collections = Collection.all.inject([]) do |result, element|
      if element.name.include?("/")
        element.name.split("/").each do |name|
          result << element if words.include?(name)
        end
      else
        result << element if words.include?(element.name)
      end
      result
    end
    collections
  end

end

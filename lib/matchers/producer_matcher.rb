module Matchers::ProducerMatcher

  def self.match_all_from(input)
    producers = Producer.where(
                "name ilike ? ", "#{input.split(/\s+/, 4)[0...3].join(' ')}%"
                )
    producers = Producer.where(
                "name ilike ? ", "#{input.split(/\s+/, 3)[0...2].join(' ')}%"
                ) unless producers.any?
    producers = Producer.where(
                "name ilike ? ", "#{input.split.first}%"
                ) unless producers.any?
    producers
  end
end

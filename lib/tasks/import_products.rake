namespace :products do

  desc "Importuj listę produktów z ceneo.pl/Plytki - użyj z [start, pages], gdzie start to początkowa strona, pages ilość stron do pobrania"
  task :fetch, [:start, :pages] => [:environment] do |t, args|
    args.with_defaults(start: 1, pages: 100)

    puts args

    begin
      agent = Mechanize.new
      page = agent.get("http://ceneo.pl/Plytki;0020-30-0-0-#{args.start.to_i - 1};0191.htm")
      args.pages.to_i.times do |page_number|
        puts "Strona: #{args.start.to_i + page_number}"
        items = page.search('div.category-list-body').first.css('.cat-prod-row-price')
        #puts items
        items.each do |item|
          Product.fetch_product(item)
        end
        sleep(rand(7))
        page = page.link_with(text: "Następna").click
      end
    rescue Exception => e
      p e
    end
  end
end

namespace :offers do

  desc "Importuj oferty"
  task fetch: :environment do
    products = Product.all.select{ |product| product.data_sets.empty? }
    products.each do |product|
      puts product
      data_set = product.data_sets.create
      data_set.offers << Offer.fetch_offers_for(product)
      p  "#{data_set.offers.count}"
      puts
      sleep(rand(7))
    end
  end

end

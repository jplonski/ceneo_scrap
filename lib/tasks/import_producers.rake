namespace :producers do
  desc "Importuj producentów"
  task fetch: :environment do
    Producer.fetch_all
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :make do
    producer nil
    name "MyString"
    url "MyText"
    page_count 1
    mask "MyText"
  end
end
